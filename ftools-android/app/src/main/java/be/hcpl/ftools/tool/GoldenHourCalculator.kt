package be.hcpl.ftools.tool

import be.hcpl.ftools.R
import be.hcpl.ftools.model.Tool

class GoldenHourCalculator : Tool() {

    override fun getName() = "Blue & Golden Hour Calculator"

    override fun getNameResourceId() = R.string.blue_and_golden_hour_tool

}
