package be.hcpl.ftools.model

abstract class Tool{

    abstract fun getName(): String

    abstract fun getNameResourceId(): Int

}
