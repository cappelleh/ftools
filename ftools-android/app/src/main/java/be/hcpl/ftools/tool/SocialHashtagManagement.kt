package be.hcpl.ftools.tool

import be.hcpl.ftools.R
import be.hcpl.ftools.model.Tool

class SocialHashtagManagement : Tool() {

    override fun getName() = "Social Hashtag Management"

    override fun getNameResourceId() = R.string.tool_social_hashtag_management

}
