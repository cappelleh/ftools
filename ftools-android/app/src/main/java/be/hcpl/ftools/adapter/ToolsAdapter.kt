package be.hcpl.ftools.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import be.hcpl.ftools.R
import be.hcpl.ftools.model.Tool
import kotlinx.android.synthetic.main.item_tool.view.*

class ToolsAdapter(val items: ArrayList<Tool>, val listener: (Tool) -> Unit) : RecyclerView.Adapter<ToolsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_tool,
            parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position], listener)

    override fun getItemCount() = items.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Tool, listener: (Tool) -> Unit) = with(itemView) {
            title.text = context.getString(item.getNameResourceId())
            setOnClickListener { listener(item) }
        }
    }

    fun showMoreItems(tools: List<Tool>) {
        items.clear()
        items.addAll(tools)
        notifyDataSetChanged()
    }

}
