package be.hcpl.android.zeronextgen.service.forecast.model

import androidx.annotation.Keep

@Keep
class ForecastDaily {

    var summary: String? = null
    var icon: String? = null
    var data: Array<DailyWeatherData>? = null

}
