package be.hcpl.ftools.tool

import be.hcpl.ftools.R
import be.hcpl.ftools.model.Tool

class MoonphaseAndExposureCalculator : Tool() {

    override fun getName() = "Moon Phase & Exposure Calculator"

    override fun getNameResourceId() = R.string.moon_phase_and_exposure_tool

}
