package be.hcpl.ftools.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import be.hcpl.ftools.R

class AboutView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        setTitle(R.string.title_about)
    }

    // TODO add about information here from raw/about.md file
}
