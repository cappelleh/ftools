package be.hcpl.ftools.tool

import be.hcpl.ftools.R
import be.hcpl.ftools.model.Tool

class ExposureReciprocationTool : Tool() {

    override fun getName() = "Exposure Reciprocation"

    override fun getNameResourceId() = R.string.exposure_reciprocation_tool

}
