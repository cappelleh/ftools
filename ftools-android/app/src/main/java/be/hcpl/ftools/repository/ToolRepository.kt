package be.hcpl.ftools.repository

import be.hcpl.ftools.model.Tool
import be.hcpl.ftools.tool.ExposureReciprocationTool
import be.hcpl.ftools.tool.GoldenHourCalculator
import be.hcpl.ftools.tool.MoonphaseAndExposureCalculator
import be.hcpl.ftools.tool.SocialHashtagManagement
import be.hcpl.ftools.tool.Sunny16Calculator

interface ToolRepository {

    fun listAllTools(): List<Tool>

}

class ToolRepositoryImpl() : ToolRepository {

    override fun listAllTools() = listOf(
        ExposureReciprocationTool(),
        GoldenHourCalculator(),
        MoonphaseAndExposureCalculator(),
        SocialHashtagManagement(),
        Sunny16Calculator()
    )

}
