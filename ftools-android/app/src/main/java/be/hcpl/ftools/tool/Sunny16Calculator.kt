package be.hcpl.ftools.tool

import be.hcpl.ftools.R
import be.hcpl.ftools.model.Tool

class Sunny16Calculator : Tool() {

    override fun getName() = "Sunny 16 Exposure Calculator"

    override fun getNameResourceId() = R.string.sunny_16_tool

}
