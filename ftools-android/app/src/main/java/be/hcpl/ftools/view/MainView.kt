package be.hcpl.ftools.view

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import be.hcpl.ftools.R
import be.hcpl.ftools.adapter.ToolsAdapter
import be.hcpl.ftools.model.Tool
import be.hcpl.ftools.repository.ToolRepositoryImpl
import kotlinx.android.synthetic.main.activity_main.*


class MainView : AppCompatActivity() {

    lateinit var toolsAdapter: ToolsAdapter
    val toolsRepository = ToolRepositoryImpl()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setTitle(R.string.app_name)

        toolsAdapter = ToolsAdapter(ArrayList(), { item ->
            // TODO onToolSelected(item)
        })

        recycler.apply {
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = toolsAdapter
        }
        retrieveTools()
    }

    private fun retrieveTools() {
        toolsAdapter.showMoreItems(toolsRepository.listAllTools())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_about, menu)
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        when (item.getItemId()) {
            R.id.about -> {
                showAboutInformation()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun showAboutInformation() {
        startActivity(Intent(this, AboutView::class.java))
    }
}
