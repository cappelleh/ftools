Welcome to F-tools, a completely free and open source Android app based on Photo Tools. Feel free to visit the project
        website at https://bitbucket.org/cappelleh/ftools/overview or download the existing (also free) Photo Tools app from the
        Google Play store to get a feeling of what this tool will become. You can find it at https://play.google.com/store/apps/details?id=be.hcpl.android.phototools. We'll talk again soon. Thanks for downloading!!
