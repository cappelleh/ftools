# About F-tools #

## The short version

In short F-tools is an App for Photographers with all kind of tools to help them with their profession
or hobby. Android version is available from [this Google Play store link](https://play.google.com/store/apps/details?id=be.hcpl.ftools).
It's the open source version created by the creator of [Photo Tools](https://play.google.com/store/apps/details?id=be.hcpl.android.phototools)

## The long version

First let me introduce myself, believe me this will make way more sense that way. I'm the creator of 
[Photo Tools](https://play.google.com/store/apps/details?id=be.hcpl.android.phototools), 
a Photography related tool that exists on Google Play for ... well I don't really know exactly
how long but for quite a long time now. 

It has over 1 million downloads and I'm aware of at least one copy that not only took the same name 
but seems to share quite some screens also. The app has been free for as long as I remember. I
did experiment with some forms of monetization like having a free version with ads next to a paid
version without ads. Then google changed the way ads should be integrated (admob) and instead of 
fixing the implementation I decided to go for a free app without ads and a paid version simply for
those that were willing to support the project. Back then I still had enough spare time to give those
that did pay for the app support so that they got something back in return for their loyalty. 

But that changed. I started a 4 year project building our home and after that we (that is 
my girlfriend and I) had a baby and ... I just didn't find enough time to work on the project and 
in response I kept only the free version and did some minimal updates, just to keep the app running. 

Literally cause at some time (what I believe is) the main feature of this app just stopped working. 
In Photo Tools you can create, maintain and (could) even share your gear (camera's and lenses) that are 
used to perform the calculations in several tools. The free service I used for sharing that information
between users (including translations) came to an end (as does all free things when they start get momentum). 

Combined with the many (breaking) Android platform changes (like runtime permissions to name one) the
app got more and more behind. And I knew what I had to do. I had to open source it so other people
with more time at least got a chance to save the app. I knew people still used it and wanted it 
to work. I still get many questions about it.

However even open sourcing the app required time to get things right, like creating the project, 
checking license options, removing signature specifics from code and so on. And even for that I 
didn't find the time. And is that even worth the effort knowing so many things still had to be fixed
and changed to get the app at full speed again?

So instead, and this is where we finally get to this project (thanks for reading if you're still there),
I decided the best thing I could do start over again from scratch and do it right this time. Or at least
better and no longer alone.

And here we are. Let me just get the first build in the store (so that package ID is mine) and we
are ready to try and improve what once was a great app. And hopefully, with some luck, lots of skills
and even more time and combined forces... we might get this F-tools app far beyond that point it's 
father, Photo Tools, ever was. 

I'm stoked and really looking forward to this. Hoping we might get at least some momentum.

## Long and short term Goals ##

For now I have only very broad descriptions of the following achievements I would like to reach
with this project. I'll update this section regularly and maybe one day I'll be able to add things
like support for iOS (who knows).

### The App ###

First goal without any doubt is to get the project up and running with some tools and some users. 
Preferrable most or at least the best tools from the Photo Tools project. 

### Form a Team ###

Next I would love to get some people interested in working together with me on this project. You 
really don't have to be a programmer. We'll also need Photographers, designers, translators, ...
 
### Go Beyond ###

And finally I hope with joined forces we manage to get a tool in the store that is better than 
the original. 

## Project Details and Contact Information ##

You can obviously find all project information on [this project website](https://bitbucket.org/cappelleh/ftools/overview) 
and the [wiki pages](https://bitbucket.org/cappelleh/ftools/wiki/) I'll be creating for it. The app 
itself is available for download from [this Google Play store link](https://play.google.com/store/apps/details?id=be.hcpl.ftools). 

If you have other questions feel free to contact me personally using my bitbucket account or by e-mail
at android@hcpl.be. 
 