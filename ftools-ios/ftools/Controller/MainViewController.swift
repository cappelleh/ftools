//
//  ViewController.swift
//  ftools
//
//  Created by Hans Cappelle on 06/01/2020.
//  Copyright © 2020 Hans Cappelle. All rights reserved.
//

import UIKit
import SwiftyJSON // for network calls

import MapKit // for location
import CoreLocation // for location

class MainViewController: UIViewController, CLLocationManagerDelegate {
    
    var mainView : MainView!
    private var weatherService = WeatherService()
    private var latitude: Double!
    private var longitude: Double!
    
    private var locationManager: CLLocationManager! = CLLocationManager()
    
    @objc func onPause() {
        // needed?
    }
    
    @objc func onResume() {
        // this is triggered whenever app is resumed, so another is shown in between
        updateLocation()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
        // these will trigger the onPause and onResume methods
        NotificationCenter.default.addObserver(self, selector: #selector(onPause), name:
            UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onResume), name:
            UIApplication.willEnterForegroundNotification, object: nil)
        
        mainView = MainView(frame: CGRect.zero)
        self.view.addSubview(mainView)
        // perform auto layout
        //mainView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets.zero)
        mainView.autoPinEdgesToSuperviewSafeArea(with: UIEdgeInsets.zero)
        
        // show location on map
        updateLocation()
    }
    
    private func updateLocation() {
        // TODO get location from preference or from GPS
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            let locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    // called when the authorization status is changed for the core location permission
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("location manager authorization status changed")
        
        // TODO check if we need to update here (in case user started without permission)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        latitude = locValue.latitude
        longitude = locValue.longitude
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            guard let city = city, let country = country, error == nil else { return }
            print(city + ", " + country)
        }
        getWeatherForecast()
    }
    
    private func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
    
    private func getWeatherForecast(){
        weatherService.forecast(
            latitude: "\(latitude.description)",
            longitude: "\(longitude.description)",
            success: { (response) -> () in self.showWeatherResult(result: response)},
            failure: { (error) -> () in self.showWeatherFetchFailed(message: "Error: \(error!.localizedDescription)")}
        )
    }
    
    private func showWeatherFetchFailed(message: String) {
        // TODO
    }
    
    private func showWeatherResult(result: JSON){
        // "temperature":2.56,"apparentTemperature":-0.7,"dewPoint":1.34,"humidity":0.92,"pressure":1009.6,"windSpeed":3.35,"windGust":8.6,"windBearing":96,"cloudCover":0.21,"uvIndex":0,"visibility":9.894,"ozone":316.7
        let summary = result["currently"]["summary"].description
        let temperature = result["currently"]["temperature"].description
        let apparentTemperature = result["currently"]["apparentTemperature"].description
        let humidity = result["currently"]["humidity"].description
        let cloudCover = result["currently"]["cloudCover"].description
        let windSpeed = result["currently"]["windSpeed"].description
        let windGust = result["currently"]["windGust"].description
        
        // TODO display temp
        //self.tempView.text = "\(Double(temperature.description)!.format(f:".0"))" // remove decimals, no precision needed for temp
        
        // icon options are icon are
        // A machine-readable text summary of this data point, suitable for selecting an icon for display. If defined, this property will have one of the following values: clear-day, clear-night, rain, snow, sleet, wind, fog, cloudy, partly-cloudy-day, or partly-cloudy-night. (Developers should ensure that a sensible default is defined, as additional values, such as hail, thunderstorm, or tornado, may be defined in the future.)
        var imageName = "questionmark.diamond"
        switch(result["currently"]["icon"].description) {
        case "clear-day" : imageName = "sun.max"
        case "clear-night" : imageName = "moon.stars"
        case "rain" : imageName = "cloud.rain"
        case "snow" : imageName = "cloud.snow"
        case "sleet" : imageName = "cloud.sleet"
        case "wind" : imageName = "wind"
        case "fog" : imageName = "cloud.fog"
        case "cloudy" : imageName = "cloud"
        case "partly-cloudy-day" : imageName = "cloud.sun"
        case "partly-cloudy-night" : imageName = "cloud.moon"
        default : imageName = "questionmark.diamond"
        }
        if #available(iOS 13.0, *) {
            //self.weatherImageView.image = UIImage(systemName: imageName)
        } else {
            // Fallback on earlier versions, see storyboard
        }
        
        // TODO
        //self.weatherResultView.text = "\(summary) with temperature \(Double(temperature ?? "0")!.format(f: ".1")) (feels like \(Double(apparentTemperature ?? "0")!.format(f: ".1")) and wind \(Double(windSpeed ?? "0")!.format(f: ".1")) (gusts up to \(Double(windGust ?? "0")!.format(f: ".1")) humidity \(Double(humidity ?? "0")!.format(f: ".1")) and cloud coverage \(Double(cloudCover ?? "0")!.format(f: ".1"))."
    }


}

